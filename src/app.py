from pathlib import Path
from time import sleep
from tkinter import PhotoImage
from tkinter import Tk
from tkinter.ttk import Button
from tkinter.ttk import Frame

# from . import cli
import cli

class LifeButton(Button):
    def __init__(self, master, root, *args, **kwargs):
        self.window = master
        super().__init__(master, *args, image=self.window.img_inactive, padding=0, **kwargs)
        self.toggle_state = -1
        self.bind("<Button-1>", self.on_click)

    def on_click(self, event=None):
        if self.cget('state') != 'disabled':
            self.toggle_state *= -1
            self.update_image()

    def update_image(self):
        if self.toggle_state == 1:
            self.config(image=self.window.img_active)
        else:
            self.config(image=self.window.img_inactive)

class LifeWindow(Frame):
    def __init__(self, root, **kwargs):
        super().__init__(**kwargs)
        self.root = root
        self.pack()
        self.img_inactive = PhotoImage(file=f"{root.images_dir / 'white.png'}")
        self.img_active = PhotoImage(file=f"{root.images_dir / 'black.png'}")
        self.life_grid = cli.Grid()
        self.grid_buttons = {}
        for (col, row) in self.life_grid.grid_state.keys():
            b = LifeButton(self, root)
            b.grid(row=row, column=col)
            self.grid_buttons[(col, row)] = b

        # Add start button.
        self.buttons = {
            'Start': {'b': None, 'c': self.run},
            'Stop': {'b': None, 'c': self.set_stop},
            'Reset': {'b': None, 'c': self.reset},
        }
        row_ct = self.life_grid.size[1]
        col_ct = self.life_grid.size[0]
        cspan = 4
        cinit = int((col_ct - cspan*3)/2)
        for i, (k, d) in enumerate(self.buttons.items()):
            d['b'] = Button(self, command=d.get('c'), text=k)
            d['b'].grid(row=row_ct, column=(cinit+cspan*i), columnspan=cspan)
        self.buttons.get('Stop').get('b').state = False

    def update_grid_buttons(self):
        print("Updating grid buttons.")
        for coord, state in self.life_grid.grid_state.items():
            b = self.grid_buttons[coord]
            if state is False:
                # b['image'] = self.img_inactive
                b.toggle_state = -1
                b.update_image()
            elif state is True:
                # b['image'] = self.img_active
                b.toggle_state = 1
                b.update_image()

    def set_stop(self, state=True):
        print(f"Setting Stop state to {state}.")
        self.buttons.get('Stop').get('b').state = state

    def reset(self):
        print("Ensuring simulation is stopped.")
        self.set_stop()
        print("Clearing grid state.")
        self.life_grid.clear_grid_state()
        self.update_grid_buttons()
        print("Allowing restart of simulation.")
        self.set_stop(False)

    def run(self):
        print("Starting simulation.")
        # self.buttons.get('Stop').get('b').state = False
        self.set_stop(False)
        # Set initial grid state.
        for coord, button in self.grid_buttons.items():
            if button.toggle_state == 1:
                self.life_grid.grid_state[coord] = True
            elif button.toggle_state == -1:
                self.life_grid.grid_state[coord] = False

        # Run simlation.
        while self.buttons.get('Stop').get('b').state is False:
            sleep(1)
            coords = self.life_grid.get_active_coords()
            self.life_grid.update_grid_state(coords)
            self.update_grid_buttons()
            self.root.update()

class LifeApp(Tk):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.images_dir = Path(__file__).resolve().parent / 'images'
        self.w = 680
        self.h = 700
        self.geometry(f"{self.w}x{self.h}")
        self.title("Conway's Game of Life")
        self.resizable(False, False)


def main():
    classname = 'LifeApp'
    root = LifeApp(className=classname)
    LifeWindow(root, class_=classname)
    root.mainloop()


if __name__ == '__main__':
    main()
