
import sys

class Grid():
    def __init__(self):
        self.size = (32, 32)
        self.active_char = '● '
        self.inactive_char = '◌ '
        self.delay = 2 # seconds
        self.grid_state = dict()
        self.clear_grid_state()

        self.active_start = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 0),
            (1, 1),
            (1, 2),
            (2, 0),
            (2, 1),
            (2, 2),
        ]
        # self.run()

    def draw_grid(self):
        for (x, y), state in self.grid_state.items():
            if state is True:
                if x < self.size[0] - 1:
                    print(self.active_char, end='')
                else:
                    print(self.active_char)
            else:
                if x < self.size[0] - 1:
                    print(self.inactive_char, end='')
                else:
                    print(self.inactive_char)

    def clear_grid_state(self):
        for y in range(self.size[1]):
            for x in range(self.size[0]):
                self.grid_state[(x, y)] = False

    def set_grid_state(self, active_coords):
        self.clear_grid_state()
        for coord in active_coords:
            self.grid_state[coord] = True

    def update_grid_state(self, active_coords):
        next_state = self.grid_state.copy()
        for coord, state in self.grid_state.items():
            # Swap state for all adjacent coords.
            adjacent_coords = self.get_adjacent_coords(coord)
            adj_ct = self.count_adjacent_actives(adjacent_coords)
            if state is True:
                if adj_ct > 1 and adj_ct < 4:
                    next_state[coord] = True
                else:
                    next_state[coord] = False
            else:
                if adj_ct == 3:
                    next_state[coord] = True
        self.grid_state = next_state

    def get_adjacent_coords(self, point):
        adjacents_raw = [
            (point[0] - 1, point[1] + 1),
            (point[0], point[1] + 1),
            (point[0] + 1, point[1] + 1),
            (point[0] - 1, point[1]),
            (point[0] + 1, point[1]),
            (point[0] - 1, point[1] - 1),
            (point[0], point[1] - 1),
            (point[0] + 1, point[1] - 1),
        ]
        adjacents = adjacents_raw.copy()
        for i, (x, y) in enumerate(adjacents_raw):
            if x < 0 or x >= self.size[0] or y < 0 or y > self.size[1]:
                # Remove point from list.
                adjacents = adjacents[:i] + adjacents[i:]
        return adjacents

    def count_adjacent_actives(self, adjacents):
        count = 0
        for coord in adjacents:
            if self.grid_state.get(coord) is True:
                count += 1
        return count

    def get_active_coords(self):
        active_coords = []
        for coord, state in self.grid_state.items():
            if state is True:
                active_coords.append(coord)
        return active_coords

    def run(self):
        self.set_grid_state(self.active_start)
        self.draw_grid()
        try:
            while True:
                input("[Enter] to continue...")
                print()
                # sleep(self.delay)
                coords = self.get_active_coords()
                self.update_grid_state(coords)
                self.draw_grid()
        except KeyboardInterrupt:
            print()
            return

def main():
    grid = Grid()
    grid.run()


if __name__ == '__main__':
    main()
